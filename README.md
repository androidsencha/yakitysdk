# YakitySDK

# How to implement


## Add Library to your project
add YakitySDK.arr into your project.

## Permission Used

Acquire required permission 

<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>

<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
<uses-permission android:name="android.permission.WAKE_LOCK"/>


<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>

<uses-permission android:name="android.permission.RECORD_AUDIO"/>
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS"/>


request permission from your app activity

 private val permissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
		
if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(this,permissions,APP_PERMISSIONS_REQ)
}
		


## Add Generala Message
Add general message 


YakityConstant.IS_PLAY_GEN_MESSAGE = true // true to play general message at the connection time
YakityConstant.GEN_MESSAGE_ID = R.raw.gm_audio // add yor message file to raw folder


## Change APP Logo

create a PNG imgae as yakadi_app_logo.png and then place it and then place it to all drawable folder.( size 512X512 )


## Change APP Background


create a PNG imgae as common_bg.png and then place it and then place it to all drawable folder.




## Launch Yakity 

startActivity(Intent(MainActivity@this,YakityLandingActivity::class.java))
